<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    
    <title>Modificacion del objeto document</title>
</head>
<body>
    <div class="pagina">
        <a href="#" class="boton" id="btnabrir">ABRIR VENTANA SECUNDARIA </a>
    </div>

    <div class="fondo_transparente">
        <div class="modal">
            <div class="modal_cerrar">
                <span>x</span>
            </div>
            <div class="modal_mensaje">
                <p>Galo Xavier Quiñonez Marin</p>
            </div>
        </div>
    </div>   
</body>
<script src="script.js"></script>
</html>


Cree una página web que establezca las siguientes propiedades del objeto Document:
–color del texto = blanco,
–color de fondo = gris y
–título del documento = “Modificaciones del objeto Document”.
–Con un botón que abra una nueva ventana secundaria q
